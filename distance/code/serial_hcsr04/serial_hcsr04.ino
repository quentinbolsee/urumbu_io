//
// serial_hcsr04.ino
//
// serial HC SR04
//
// Quentin Bolsee 11/29/22
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#define PIN_LED_A 4
#define PIN_LED_C 2

#define PIN_TRIG 14
#define PIN_ECHO 15

// timeout after ~1 meter
#define PULSE_TIMEOUT 5000

#define VAL_MIN 300
#define VAL_MAX 3000


void setup() {
  pinMode(PIN_LED_A, OUTPUT);
  digitalWrite(PIN_LED_A, HIGH);
  pinMode(PIN_LED_C, OUTPUT);
  digitalWrite(PIN_LED_C, LOW);
  pinMode(PIN_TRIG, OUTPUT);
  pinMode(PIN_ECHO, INPUT);
  SerialUSB.begin(115200);
}


void print_16bit(int v) {
  const char bfr[] = {v & 0xFF, (v >> 8) & 0xFF};
  SerialUSB.write(bfr, 2);
}


long int measure_dist() {
  delayMicroseconds(2);
  digitalWrite(PIN_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_TRIG, LOW);

  long int duration = pulseIn(PIN_ECHO, HIGH, PULSE_TIMEOUT);
  
  if (duration == 0) {
    duration = PULSE_TIMEOUT;
  }

  // in tens of mm
  long int distance_01mm = duration * 3.4 / 2;

  return distance_01mm;
}


void loop() {
  if (SerialUSB.available()) {
    char c = SerialUSB.read();
    if (c == '?') {
      int value = measure_dist();
      int value_10bit = map(value, VAL_MIN, VAL_MAX, 1023, 0);
      value_10bit = max(min(value_10bit, 1023), 0);
      print_16bit(value_10bit);
    } else if (c == '@') {
      SerialUSB.write("0005");
    }
  }
}
