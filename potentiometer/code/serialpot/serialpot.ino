//
// serailpot.ino
//
// serial potentiometer sensor
//
// Quentin Bolsee 11/29/22
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#define PIN_LED_A 4
#define PIN_LED_C 2
#define PIN_SENSOR 14


void setup() {
  pinMode(PIN_LED_A, OUTPUT);
  digitalWrite(PIN_LED_A, HIGH);
  pinMode(PIN_LED_C, OUTPUT);
  digitalWrite(PIN_LED_C, LOW);
  pinMode(PIN_SENSOR, INPUT);
  SerialUSB.begin(115200);
}


void print_16bit(int v) {
  const char bfr[] = {v & 0xFF, (v >> 8) & 0xFF};
  SerialUSB.write(bfr, 2);
}


void loop() {
  if (SerialUSB.available()) {
    char c = SerialUSB.read();
    if (c == '?') {
      int value_10bit = analogRead(PIN_SENSOR);
      print_16bit(value_10bit);
    } else if (c == '@') {
      SerialUSB.write("0004");
    }
  }
}
