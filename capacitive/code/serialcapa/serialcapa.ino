//
// serialcapa.ino
//
// serial capacitive sensing
//
// Quentin Bolsee 11/29/22
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#include "Adafruit_FreeTouch.h"

#define PIN_LED_A 4
#define PIN_LED_C 2
#define PIN_CAPA 5

#define OVERSAMPLE 64

// for a 64 oversampling
#define VAL_MIN 13700
#define VAL_MAX 15000


Adafruit_FreeTouch qt(PIN_CAPA, OVERSAMPLE_4, RESISTOR_50K, FREQ_MODE_NONE);


void setup() {
  pinMode(PIN_LED_A, OUTPUT);
  digitalWrite(PIN_LED_A, HIGH);
  pinMode(PIN_LED_C, OUTPUT);
  digitalWrite(PIN_LED_C, LOW);
  SerialUSB.begin(115200);

  qt.begin();
}


void print_16bit(int v) {
  const char bfr[] = {v & 0xFF, (v >> 8) & 0xFF};
  SerialUSB.write(bfr, 2);
}


int measure_capa() {
  long int value_over = 0;

  for (int i = 0; i < OVERSAMPLE; i++) {
    value_over += qt.measure();
  }

  return value_over;
}


void loop() {
  if (SerialUSB.available()) {
    char c = SerialUSB.read();
    if (c == '?') {
      int value = measure_capa();
      int value_10bit = map(value, VAL_MIN, VAL_MAX, 0, 1023);
      value_10bit = max(min(value_10bit, 1023), 0);
      print_16bit(value_10bit);
    } else if (c == '@') {
      SerialUSB.write("0003");
    }
  }
}
