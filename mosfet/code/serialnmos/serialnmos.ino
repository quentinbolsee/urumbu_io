//
// serialnmos.ino
//
// serial NMOS gate
//
// Quentin Bolsee 11/29/22
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#define PIN_LED_A 4
#define PIN_LED_C 2
#define PIN_GATE 5

void setup() {
  // put your setup code here, to run once:
  pinMode(PIN_LED_A, OUTPUT);
  digitalWrite(PIN_LED_A, HIGH);
  pinMode(PIN_LED_C, OUTPUT);
  digitalWrite(PIN_LED_C, LOW);
  pinMode(PIN_GATE, OUTPUT);
  digitalWrite(PIN_GATE, LOW);
  SerialUSB.begin(115200);
}

void loop() {
  if (SerialUSB.available()) {
    char c = SerialUSB.read();
    if (c == '1') {
      digitalWrite(PIN_GATE, HIGH);
    } else if (c == '0') {
      digitalWrite(PIN_GATE, LOW);
    } else if (c == '@') {
      SerialUSB.write("0007");
    }
  }
}
