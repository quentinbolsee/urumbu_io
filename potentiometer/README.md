# Urumbu potentiometer input

![](./img/D11C.potentiometer.components.top.jpg)

![](./img/D11C.potentiometer.components.bottom.jpg)

This board returns a 10-bit value depending on the potentiometer.

## Code

The firmware be found [here](./code/d11_pot/d11_pot.ino).

## KiCad files

- [project](./samd11c14_pot.kicad_pro)
- [schematic](./samd11c14_pot.kicad_sch)
- [board](./samd11c14_pot.kicad_pcb)

## .png files

![](./img/samd11c14_pot-composite.png)

![](./img/samd11c14_pot-F_Cu.png)

![](./img/samd11c14_pot-Edge_Cuts.png)

![](./img/samd11c14_pot-interior_traces_comp.png)
