# Urumbu capacitive input

<img src=./img/D11C.mosfet.components.jpg width=40%><br>

This board returns a 10-bit value depending on the capacitive sensing pad reading.

## Code

The firmware can be found [here](./code/serialnmos/serialnmos.ino).

## pcb.py file

- [board](./hello.NMOS-D11C)

## .png files

<img src=./img/hello.NMOS-D11C.board.png width=30%><br>

Traces:

<img src=./img/hello.NMOS-D11C.traces.png width=30%><br>

Traces + exterior:

<img src=./img/hello.NMOS-D11C.traces_exterior.png width=30%><br>

Interior:

<img src=./img/hello.NMOS-D11C.interior.png width=30%><br>
