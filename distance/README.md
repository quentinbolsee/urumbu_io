# Urumbu distance input

![](./img/D11C.distance.components.top.jpg)

![](./img/D11C.distance.components.bottom.jpg)

This board returns a 10-bit value depending on the distance read by the HC-SR04 sensor.

## Code

The firmware can be found [here](./code/d11_distance/d11_distance.ino).

## KiCad files

- [project](./samd11c14_hc_sr04.kicad_pro)
- [schematic](./samd11c14_hc_sr04.kicad_sch)
- [board](./samd11c14_hc_sr04.kicad_pcb)

## .png files

![](./img/samd11c14_hc_sr04-composite.png)

![](./img/samd11c14_hc_sr04-F_Cu.png)

![](./img/samd11c14_hc_sr04-Edge_Cuts.png)

![](./img/samd11c14_hc_sr04-interior_traces_comp.png)
