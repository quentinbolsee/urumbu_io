# Urumbu capacitive input

<img src=./img/D11C.ADC10k.components.jpg width=40%><br>

This board returns a 10-bit value based on an ADC pin reading from a voltage divider between a 10k resistor and a variable resistor device the user can plug in. Examples include:

- NTC 100k thermistor
- Force sensor

## Code

The firmware can be found [here](./code/serialadc/serialadc.ino).

## pcb.py file

- [board](./hello.ADC10k-D11C)

## .png files

<img src=./img/hello.ADC10k-D11C.board.png width=30%><br>

Traces:

<img src=./img/hello.ADC10k-D11C.traces.png width=30%><br>

Traces + exterior:

<img src=./img/hello.ADC10k-D11C.traces_exterior.png width=30%><br>

Interior:

<img src=./img/hello.ADC10k-D11C.interior.png width=30%><br>
