# Urumbu I/O modules

## Inputs

### [Capacitive input](./capacitive)

<img src=./capacitive/img/D11C.capacitive.components.top.jpg width=45%><br>

### [Distance input](./distance)

<img src=./distance/img/D11C.distance.components.top.jpg width=45%><img src=./distance/img/D11C.distance.components.bottom.jpg width=45%><br>

### [ADC+10k input](./adc_10k)

<img src=./adc_10k/img/D11C.ADC10k.components.jpg width=35%><br>

### [Potentiometer input](./potentiometer)

<img src=./potentiometer/img/D11C.potentiometer.components.top.jpg width=45%><img src=./potentiometer/img/D11C.potentiometer.components.bottom.jpg width=45%><br>

## Outputs

### [MOSFET output](./mosfet)

<img src=./mosfet/img/D11C.mosfet.components.jpg width=35%><br>

### [Stepper output](https://gitlab.cba.mit.edu/neilg/urumbu/-/tree/master/serialstep)

<img src=./extra/img/D11C.stepper.components.jpg width=35%><br>

### [Servo output](https://gitlab.cba.mit.edu/neilg/urumbu/-/tree/master/serialservo)

<img src=./extra/img/D11C.servo.components.jpg width=35%><br>
