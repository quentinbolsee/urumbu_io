# Urumbu capacitive input

<img src=./img/D11C.capacitive.components.top.jpg width=40%><br>

This board returns a 10-bit value depending on the capacitive sensing pad reading.

## Code

The firmware can be found [here](./code/d11_capa/d11_capa.ino).

## KiCad files

- [project](./samd11c14_capa.kicad_pro)
- [schematic](./samd11c14_capa.kicad_sch)
- [board](./samd11c14_capa.kicad_pcb)

## .png files

<img src=./img/samd11c14_capa-composite.png width=30%><br>

Traces:

<img src=./img/samd11c14_capa-F_Cu.png width=30%><br>

Interior:

<img src=./img/samd11c14_capa-Edge_Cuts.png width=30%><br>

Traces + exterior:

<img src=./img/samd11c14_capa-interior_traces_comp.png width=30%><br>
